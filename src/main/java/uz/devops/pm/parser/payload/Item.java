package uz.devops.pm.parser.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Item {
    private String title;

    @JsonIgnore
    private Item parent;

    public String makeQuery() {
        return ".item[?(@.name==\""+this.getTitle()+"\")]";
    }

}
