package uz.devops.pm.parser.payload;

import java.util.ArrayList;
import java.util.List;

public class FolderItem extends  Item {
    private List<Item> children;

    public void addItem(Item item) {
        List<Item> children = this.getChildren();
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(item);
        this.children = children;
    }

    public List<Item> getChildren() {
        return this.children;
    }
}
