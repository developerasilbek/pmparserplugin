package uz.devops.pm.parser.payload.enumeration;

@SuppressWarnings("unused")
public enum MethodType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
}
