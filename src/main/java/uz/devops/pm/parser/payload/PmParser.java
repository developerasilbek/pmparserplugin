package uz.devops.pm.parser.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.devops.pm.parser.payload.enumeration.MethodType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PmParser {
    private String url;

    private String username;

    private String password;

    private String token;

    private String body;

    private String headers;

    private MethodType httpMethodType;

    private String requestMediaType;

    private String responseMediaType;

    private String successCode;

}
