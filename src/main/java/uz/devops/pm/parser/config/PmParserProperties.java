package uz.devops.pm.parser.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Setter
@Getter
@ConfigurationProperties(prefix = "pm-parser")
public class PmParserProperties {

    private Map<String, String> structure;

}
