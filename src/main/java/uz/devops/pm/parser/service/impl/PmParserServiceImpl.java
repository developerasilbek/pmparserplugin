package uz.devops.pm.parser.service.impl;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.config.PmParserProperties;
import uz.devops.pm.parser.payload.FolderItem;
import uz.devops.pm.parser.payload.Item;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.payload.RequestItem;
import uz.devops.pm.parser.service.PmParserService;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import static uz.devops.pm.parser.service.PmParserService.NAME;

@Slf4j
@Service(NAME)
@ConditionalOnProperty(
    prefix = "pm-parser",
    name = "simulate",
    havingValue = "false"
)
public class PmParserServiceImpl implements PmParserService {

    private final PmParserProperties pmParserProperties;

    @Autowired
    public PmParserServiceImpl(PmParserProperties pmParserProperties) {
        this.pmParserProperties = pmParserProperties;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PmParser> parse(MultipartFile file, List<Item> items) throws IOException, IllegalAccessException {
        log.info("Multipart file to parse : {}", file);
        List<PmParser> pmParserList = new ArrayList<>();

        Configuration conf = Configuration
            .defaultConfiguration()
            .addOptions(Option.SUPPRESS_EXCEPTIONS, Option.ALWAYS_RETURN_LIST);
        DocumentContext ctx = JsonPath.using(conf).parse(file.getInputStream());

        Queue<Item> itemQueue = new ArrayDeque<>();
        Queue<String> queryQueue = new ArrayDeque<>();

        for (Item item : items) {
            itemQueue.add(item);
            queryQueue.add(item.makeQuery());
        }

        while (!itemQueue.isEmpty()) {
            Item item = itemQueue.remove();
            String query = queryQueue.remove();

            if (item instanceof FolderItem) {
                FolderItem folderItem = (FolderItem) item;
                for (Item i : folderItem.getChildren()) {
                    itemQueue.add(i);
                    queryQueue.add(query + i.makeQuery());
                }
            } else {
                PmParser pmParser = getFromFile(ctx, query);
                if (pmParser.getUrl() != null) {
                    pmParserList.add(pmParser);
                }
            }
        }

        return pmParserList;
    }

    @Override
    public List<Item> getHierarchy(MultipartFile file) throws IOException {
        log.info("Multipart file to get hierarchy : {}", file);
        List<Item> items = new ArrayList<>();

        Configuration conf = Configuration
            .defaultConfiguration()
            .addOptions(Option.SUPPRESS_EXCEPTIONS, Option.ALWAYS_RETURN_LIST);
        DocumentContext ctx = JsonPath.using(conf).parse(file.getInputStream());

        Item item;
        Queue<String> queue = new ArrayDeque<>();
        Queue<Item> itemsQueue = new ArrayDeque<>();
        queue.add("$.item[?(@.request)].name");
        itemsQueue.add(new Item("", null));

        // BFS => Breadth first Search
        while (!queue.isEmpty()) {
            String query = queue.remove();
            item = itemsQueue.remove();
            List<String> children = ctx.read(query);
            for (String name : children) {
                RequestItem requestItem = new RequestItem();
                requestItem.setTitle(name);
                if (!item.getTitle().equals("")) {
                    FolderItem folder = (FolderItem) item;
                    folder.addItem(requestItem);
                } else {
                    items.add(requestItem);
                }
            }

            query = query.replace("?(@.request)", "?(@.item)");
            children = ctx.read(query);
            for (String name : children) {
                FolderItem folderItem = new FolderItem();
                folderItem.setTitle(name);
                if (!item.getTitle().equals("")) {
                    FolderItem folder = (FolderItem) item;
                    folder.addItem(folderItem);
                } else {
                    items.add(folderItem);
                }
                String folderQuery = query.replace("?(@.item)", "?(@.item && @.name==\"" + name + "\")")
                    .replace("].name", "]")
                    + ".item[?(@.request)].name";
                itemsQueue.add(folderItem);
                queue.add(folderQuery);
            }
        }

        return items;
    }

    private PmParser getFromFile(DocumentContext ctx, String query) throws IllegalAccessException {


        PmParser pmParser = new PmParser();
        Field[] fields = pmParser.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            String pmParserPath = pmParserProperties
                .getStructure()
                .get(name);
            List<Object> objectList;
            if (pmParserPath != null) {
                objectList = ctx.read(query + "." + pmParserPath);
            } else {
                log.info("Field value not exists : {}", field);
                continue;
            }

            Optional<Object> first = objectList
                .stream()
                .findFirst();
            String result;
            if (first.isPresent()) {
                result = first.get().toString();
            } else {
                log.info("Field value not exists : {}", field);
                continue;
            }

            if (field.getType().isEnum()) {
                try {
                    //noinspection rawtypes
                    field.set(pmParser, Enum.valueOf((Class<Enum>) field.getType(), result.toUpperCase()));
                } catch (Exception e) {
                    log.info("Enum not contains the same value : {}", result);
                }
            } else if (field.getType().equals(String.class)) {
                field.set(pmParser, result);
            } else if (field.getType().equals(Integer.class)) {
                try {
                    field.set(pmParser, Integer.valueOf(result));
                } catch (Exception e) {
                    log.info("Integer not contains the same value : {}", result);
                }
            } else if (field.getType().equals(Boolean.class)) {
                field.set(pmParser, Boolean.valueOf(result));
            } else {
                log.warn("Field type not found : {}", field);
            }

            field.setAccessible(false);
        }
        return pmParser;
    }
}
