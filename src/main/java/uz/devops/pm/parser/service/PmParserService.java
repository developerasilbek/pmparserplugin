package uz.devops.pm.parser.service;

import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.payload.Item;
import uz.devops.pm.parser.payload.PmParser;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("unused")
public interface PmParserService {

    String NAME = "pmParserService";

    List<PmParser> parse(MultipartFile file, List<Item> pathList) throws IOException, IllegalAccessException;

    List<Item> getHierarchy(MultipartFile file) throws IOException;

}
