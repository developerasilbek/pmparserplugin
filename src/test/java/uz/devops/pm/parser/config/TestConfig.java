package uz.devops.pm.parser.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("uz.devops.pm.parser")
public class TestConfig {

}
