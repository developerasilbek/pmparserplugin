package uz.devops.pm.parser.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.config.TestConfig;
import uz.devops.pm.parser.payload.FolderItem;
import uz.devops.pm.parser.payload.Item;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.payload.RequestItem;
import uz.devops.pm.parser.payload.enumeration.MethodType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfig.class})
public class PmParserServiceImplTest {

    @Autowired
    private PmParserServiceImpl pmParserService;

    private MultipartFile correctFile;
    private MultipartFile correctFile2;

    private static final String SAMPLE_URL = "https://www.devops.uz";
    private static final String SAMPLE_USERNAME = "admin";
    private static final String SAMPLE_PASSWORD = "admin";
    private static final MethodType SAMPLE_METHOD_TYPE = MethodType.GET;


    private static final String SAMPLE_URL2 = "http://localhost:9000/api/gateway-params?page=0&size=20";
    private static final String SAMPLE_TOKEN2 = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTY3MjEyMDM1M30.edsjiQUyFfSRE3UZunt8jB52smU04uQr5A2dagdjbMN-nApEIeTFHjNDhBwLAmMOLWEyHzTCsT7yo5vHklDt7g";

    private static final String SAMPLE_BODY2 = "{mode=formdata, formdata=[" +
        "{\"key\":\"type\",\"value\":\"pay\",\"type\":\"text\"}" +
        ",{\"key\":\"mode\",\"value\":\"prod\",\"type\":\"text\"}" +
        ",{\"key\":\"active\",\"value\":\"true\",\"type\":\"text\"}" +
        "]}";

    private static final MethodType SAMPLE_METHOD_TYPE2 = MethodType.GET;

    @Before
    public void setUp() throws IOException {

        correctFile = getMultipartFile("src/test/resources/TestPostmanCollection.postman_collection.json");
        correctFile2 = getMultipartFile("src/test/resources/TestPostmanCollection2.postman_collection.json");

    }

    @Test
    public void testCreatePmParserFromFileShouldSucceed() throws IOException, IllegalAccessException {
        List<Item> pathList = new ArrayList<>();
        FolderItem item = new FolderItem();
        item.setTitle("folder1");
        RequestItem requestItem = new RequestItem();
        requestItem.setTitle("New Request");
        item.addItem(requestItem);
        pathList.add(item);
        List<PmParser> underTest = pmParserService.parse(correctFile, pathList);
        assertNotNull(underTest);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getUrl(), SAMPLE_URL);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getUsername(), SAMPLE_USERNAME);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getPassword(), SAMPLE_PASSWORD);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getHttpMethodType(), SAMPLE_METHOD_TYPE);

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @Test
    public void testCreatePmParserFromFileShouldFail() throws IOException, IllegalAccessException {
        List<Item> pathList = new ArrayList<>();
        FolderItem item = new FolderItem();
        item.setTitle("test");
        RequestItem requestItem = new RequestItem();
        requestItem.setTitle("wrong_request");
        item.addItem(requestItem);
        pathList.add(item);
        List<PmParser> underTest = pmParserService.parse(correctFile, pathList);
        assertTrue(underTest.isEmpty());

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @Test
    public void testCreatePmParserFromFile2ShouldSucceed() throws IOException, IllegalAccessException {
        List<Item> pathList = new ArrayList<>();
        FolderItem item = new FolderItem();
        item.setTitle("folder1");
        RequestItem requestItem = new RequestItem();
        requestItem.setTitle("gatewayRequest");
        item.addItem(requestItem);
        pathList.add(item);
        FolderItem item2 = new FolderItem();
        item2.setTitle("folder2");
        RequestItem requestItem2 = new RequestItem();
        requestItem2.setTitle("asilbek");
        item2.addItem(requestItem2);
        pathList.add(item2);
        List<PmParser> underTest = pmParserService.parse(correctFile2, pathList);
        assertNotNull(underTest);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getUrl(), SAMPLE_URL2);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getToken(), SAMPLE_TOKEN2);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getBody(), SAMPLE_BODY2);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getHttpMethodType(), SAMPLE_METHOD_TYPE2);

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @Test
    public void testCreatePmParserFromFile2ShouldFail() throws IOException, IllegalAccessException {
        List<Item> pathList = new ArrayList<>();
        FolderItem item = new FolderItem();
        item.setTitle("wrong_folder");
        RequestItem requestItem = new RequestItem();
        requestItem.setTitle("wrong_request");
        item.addItem(requestItem);
        pathList.add(item);
        FolderItem item2 = new FolderItem();
        item2.setTitle("wrong_folder_2");
        RequestItem requestItem2 = new RequestItem();
        requestItem2.setTitle("wrong_request_2");
        item2.addItem(requestItem2);
        pathList.add(item2);
        List<PmParser> underTest = pmParserService.parse(correctFile2, pathList);
        assertTrue(underTest.isEmpty());

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static MultipartFile getMultipartFile(String path) throws IOException {
        File file = new File(path);
        byte[] bytes = new byte[(int) file.length()];

        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(bytes);
        }
        return new MockMultipartFile("file",
            file.getName(), "text/plain", bytes);
    }
}
